import pkg from './package.json';

let banner = `/*! ${pkg.name} v${pkg.version} | ${pkg.description} | Copyright ${new Date().getFullYear()} | ${pkg.license} license */`;
let formats = ['iife', 'es', 'cjs'];

export default formats.map(function (format) {
  return {
    input: './src/js/index.js',
    output: {
      file: `./dist/js/time${format === 'iife' ? '' : `.${format}`}.js`,
      format: format,
      name: 'Time',
      banner: banner
    }
  }
});
