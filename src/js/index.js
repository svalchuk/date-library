function emitEvent (type, detail = {}, elem = document) {

  // Make sure there's an event type
  if (!type) return;

  // Create a new event
  let event = new CustomEvent(type, {
    bubbles: true,
    cancelable: true,
    detail: detail
  });

  // Dispatch the event
  return elem.dispatchEvent(event);
}

/**
 * The Constructor object
 * @param {Array} date A date string, object, or array of arguments
 */
function Constructor(date = [], options = {}) {
  if (!Array.isArray(date)) {
    date = [date];
  }

  let settings = Object.assign({
    days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
  }, options);

  Object.freeze(settings);

  Object.defineProperties(this, {
    date: {value: new Date(...date)},
    _settings: {value: settings}
  });

  // Emit a time:ready event
  emitEvent('time:ready', {
    time: this
  })
}

/**
 * Get name of the day
 * @returns {String}      Name of the day
 */
Constructor.prototype.getDay = function () {
  return this._settings.days[this.date.getDay()];
}

/**
 * Get name of the month
 * @returns {String}     Name of the month
 */
Constructor.prototype.getMonth = function () {
  return this._settings.months[this.date.getMonth()];
}

/**
 * Add seconds to a date
 * @param   {Number}  number  Seconds to add
 * @returns {Date}            Date
 */
Constructor.prototype.addSeconds = function (number) {
  let d = new Date(this.date);
  d.setSeconds(d.getSeconds() + number);
  let time = new Constructor(d, this._settings);

  // Emit time:update event
  let cancel = !emitEvent('time:update', {
    time,
    type: 'seconds',
    amount: number
  });

  // If canceled, return a copy of the original instance
  // If not, return new instance
  if (cancel) return new Constructor(this.date, this._settings);
  return time;
}

/**
 * Add minutes to a date
 * @param   {Number}  number  Minutes to add
 * @returns {Date}            Date
 */
Constructor.prototype.addMinutes = function (number) {
  let d = new Date(this.date);
  d.setMinutes(d.getMinutes() + number);
  let time = new Constructor(d, this._settings);

  // Emit time:update event
  let cancel = !emitEvent('time:update', {
    time,
    type: 'minutes',
    amount: number
  });

  // If canceled, return a copy of the original instance
  // If not, return new instance
  if (cancel) return new Constructor(this.date, this._settings);
  return time;
}

/**
 * Add hours to a date
 * @param   {Number}  number  Hours to add
 * @returns {Date}            Date
 */
Constructor.prototype.addHours = function (number) {
  let d = new Date(this.date);
  d.setHours(d.getHours() + number);
  let time = new Constructor(d, this._settings);

  // Emit time:update event
  let cancel = !emitEvent('time:update', {
    time,
    type: 'hours',
    amount: number
  });

  // If canceled, return a copy of the original instance
  // If not, return new instance
  if (cancel) return new Constructor(this.date, this._settings);
  return time;
}

/**
 * Add days to a date
 * @param   {Number}  number  Days to add
 * @returns {Date}            Date
 */
Constructor.prototype.addDays =function (number) {
  let d = new Date(this.date);
  d.setDate(d.getDate() + number);
  let time = new Constructor(d, this._settings);

  // Emit time:update event
  let cancel = !emitEvent('time:update', {
    time,
    type: 'days',
    amount: number
  });

  // If canceled, return a copy of the original instance
  // If not, return new instance
  if (cancel) return new Constructor(this.date, this._settings);
  return time;
}

/**
 * Add months to a date
 * @param   {Number} number Months to add
 * @returns {Date}          Date
 */
Constructor.prototype.addMonths = function (number) {
  let d = new Date(this.date);
  d.setMonth(d.getMonth() + number);
  let time = new Constructor(d, this._settings);

  // Emit time:update event
  let cancel = !emitEvent('time:update', {
    time,
    type: 'month',
    amount: number
  });

  // If canceled, return a copy of the original instance
  // If not, return new instance
  if (cancel) return new Constructor(this.date, this._settings);
  return time;
}

/**
 * Add years to a date
 * @param   {Date}   date   Date
 * @param   {Number} number Years to add
 * @returns {Date}          Date
 */
Constructor.prototype.addYears = function (number) {
  let d = new Date(this.date);
  d.setFullYear(d.getFullYear() + number);
  let time = new Constructor(d, this._settings);

  // Emit time:update event
  let cancel = !emitEvent('time:update', {
    time,
    type: 'years',
    amount: number
  });

  // If canceled, return a copy of the original instance
  // If not, return new instance
  if (cancel) return new Constructor(this.date, this._settings);
  return time;
}

export default Constructor;
